<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/greeting', function () {
    return view ('HomePage.greeting');
});


Route::get('/refreshToken',['middleware' => 'jwt.refresh', 'uses'=>'AuthController@refreshToken'], function (Request $request) {

});
//Autentification


Route::get('/getContacts',['middleware' => 'jwt.auth','uses'=>'UserDataController@getContacts'], function (Request $request) {

});
Route::get('/sendSMS',['middleware' => 'jwt.auth','uses'=>'UserDataController@sendSMS'], function (Request $request) {

});
Route::post('/validate',['middleware' => 'jwt.auth','uses'=>'UserDataController@validate'], function (Request $request) {

});
Route::post('/getRegistredContacts',['middleware' => 'jwt.auth','uses'=>'UserDataController@validate'], function (Request $request) {

});

Route::post('/signin','AuthController@signin', function (Request $request) {

});

Route::get('/user',['middleware' => 'jwt.auth','uses'=>'AuthController@show'], function (Request $request) {

});

Route::post('/newClient','UserDataController@newClient', function (Request $request, Response $response) {
    return $response;
});