<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

//Route::post('/getName',['middleware' => 'jwt.auth','uses'=>'AuthController@getName'], function (Request $request) {

//});

//---------Autentification
Route::post('/autentification','AuthController@autentification', function (Request $request) {

});
Route::post('/signIn','AuthController@signin', function (Request $request) {

});
Route::post('/refreshToken',['middleware' => 'jwt.refresh','uses'=>'AuthController@refreshToken'], function (Request $request) {

});
//Route::post('/refreshToken',['middleware' => 'jwt.refresh', function() {}]);
//---------Autentification

//---------Registration
Route::post('/sendSMS',['middleware' => 'jwt.auth','uses'=>'UserDataController@sendSMS'], function (Request $request) {

});
Route::post('/validateClient',['middleware' => 'jwt.auth','uses'=>'AuthController@validateClient'], function (Request $request) {

});
//---------Registration

Route::post('/getName',['middleware' => 'jwt.auth','uses'=>'AuthController@getName'], function (Request $request) {

});
Route::post('/getContacts','AuthController@getContacts', function (Request $request)  {

});