<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    public function ownerUser()
    {
        return $this->belongsTo('App\User', 'iduser', 'id');
    }

    public function contactUser()
    {
        return $this->belongsTo('App\User', 'idcontact', 'id');
    }
}
