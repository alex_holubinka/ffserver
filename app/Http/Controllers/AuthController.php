<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.12.2016
 * Time: 21:22
 */

namespace App\Http\Controllers;

use Response;
use App\Http\Controllers\Controller;
use App\User;

use Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;




class AuthController extends Controller
{
    public function autentification(Request $request, Response $response){

        $credentials = $request::only('phone','password','name');
       try {

           if (User::where('phone', $request::only('phone'))->get()){
               return Response::json(['error' => 'user_duplicate'], 409);
           } else {

           }
           $user = User::firstOrCreate($credentials);

        } catch (Exception $e) {
           return Response::json(['error' => 'user_error'], 500);
           //return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
        }

        $token = JWTAuth::fromUser($user);

        return Response::json(compact('token'));
    }

    public function signIn(Request $request, Response $response){

        $credentials = $request::only('phone','password','name');

        if ( ! $token = JWTAuth::attempt($credentials)) {
            //return Response::HTTP_UNAUTHORIZED; //HTTP_UNAUTHORIZED
            return Response::json(false, 401);
        }

        return Response::json(compact('token'));

    }

    //AuthController
    public function refreshToken(){
        $token = JWTAuth::getToken();
        if(!$token){
            throw new BadRequestHtttpException('Token not provided');
        }
        try{
           $newToken = JWTAuth::refresh($token);
        }catch(TokenInvalidException $e){
            throw new AccessDeniedHttpException('The token is invalid');
        }

        return Response::json(compact('newToken'));
    }

    public function getName(Request $request){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $d = JWTAuth::getPayload($token);
        $users = User::all();
        $user1 = User::find(1);

        return Response::json([
            'data' => [
                'email' => $user->email,
                'registered_at' => $user->created_at->toDateTimeString()
            ]
        ]);
    }
    public function show(Request $request){
        return JWTAuth::parseToken()->toUser() ;

    }
    public function getContacts(Request $request){
        $user1 = User::find(1);
        $contacts = $user1->contacts;
        /*
        $users = array();
        foreach ($contacts as $contact){
            //$userOwn = $contact->ownerUser;
            $userCont = $contact->contactUser;
            array_push($users, $userCont);
        }
        */
        return  $contacts;//$users;//$users;

    }
}